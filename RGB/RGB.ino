#define redl 7 //pin красный
#define greenl 6 //pin зеленый
#define bluel 5  //pin синий
int r,g,b; 

void setup() { 
//Выводим список цветов
Serial.println("color"); 
Serial.println("red, green, blue, white, yellow, pink, light blue."); 
Serial.begin(9600); 
} 

void make_color(int red, int green, int blue) { //функция формирования цветов 
analogWrite(redl, red); 
analogWrite(greenl, green); 
analogWrite(bluel, blue); 
r=red; 
g=green; 
b=blue; 
} 

void make_color(String color) { 
if (color==("red")) 
make_color(255, 0, 0); 
else if (color == "green") 
make_color(0, 255, 0); 
else if (color == "blue") 
make_color(0, 0, 255); 
else if (color == "white") 
make_color(255, 255, 255); 
else if (color == "yellow") 
make_color(244, 232, 66); 
else if (color == "pink") 
make_color(214, 7, 80); 
else if (color == "light blue") 
make_color(66, 244, 229); 

delay(100); 
} 

void loop() { 
String input; 
input = Serial.readString(); 
make_color(input); 

}
